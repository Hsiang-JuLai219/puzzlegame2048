/*===============================================================*/
/* File Name: AdvancedUser.h							         */
/* Author: Hsiang-Ju Lai									     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: The header file defining Class AdvancedUser,	 */
/*	which has the game control functions for Game 2048. This	 */
/*	class is a subclass of Class User.							 */
/*===============================================================*/

#pragma once
#include <stack>
#include <string>
#include <vector>
#include <fstream>
#include "User.h"
#include "RankingList.h"
using namespace std;

class AdvancedUser : public User
{
private:
	stack<GameData> history;
	vector<vector<int>> records;
	string username;
	RankingList *rankingList;

	void goBack();
	void updateHistory(int score, int step);
	void loadHistory();
	void displayHistory();
	void showBestScore();
	void startGame();
	int findBestScore();
	bool isBestScore(int score, int steps);
	
public:
	AdvancedUser(string username, RankingList *rankingList);
	void userMenu();
	
	

};