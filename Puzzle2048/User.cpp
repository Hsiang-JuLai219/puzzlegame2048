/*===============================================================*/
/* File Name: User.cpp									         */
/* Author: Hsiang-Ju Lai, Eric Hsu							     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: Implementation of Class User				     */
/*===============================================================*/

#include "User.h"
using namespace std;

User::User():isGoBackAvailable(false){}

//display game screen on console
//go back function is visible when passed-in param is true
void User::displayGame(bool isGobackable)
{
	system("cls");
	
	printLine();
	cout << endl << setw(21) << "Score to Win: " << TARGET_SCORE
		<< setw(20) << "Current Score: " << data->getScore()
		<< setw(20) << "Current Step: " << data->getStep() << endl;
	printLine();
	cout << endl << endl;
	data->printArray();
	cout << endl;
	printLine();
	if(isGobackable)
		cout << endl << setw(10) << "w - Up" << setw(11) << "s - Down"
		<< setw(11) << "a - Left" << setw(12) << "d - Right" << setw(13) << "b - GoBack"
		<< setw(11) << "p - Exit" << endl;
	else 
		cout << endl << setw(14) << "w - Up" << setw(13) << "s - Down"
		<< setw(13) << "a - Left" << setw(14) << "d - Right"
		<< setw(13) << "p - Exit" << endl;
	printLine();
	cout << endl;
}

//pring a line
void User::printLine()
{
	cout << "_________________________________________________________"
		<< "_______________" << endl;
}

//display the user menu
void User::userMenu()
{
	string input;
	while (true)
	{
		system("cls");
		cout << "\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
		cout << setw(46) << "Trial User\n";
		cout << "   ___________________________________________________________________";
		cout << "\n\n";
		cout << "				1 ->> Start Game\n\n";
		cout << "				2 ->> Exit\n\n";
		cout << "\n\n\n\n\n\n\n\n" << endl;

		cin >> input;

		switch (input[0])
		{
		case '1':
			startGame();
			break;
		case '2': return;
			break;
		}
	}
}

//start the game
void User::startGame()
{
	bool isOver(false), isMoved(false);
	string input;
	int direction;
	data = new GameData();
	
	while (!isOver)
	{	
		
		displayGame(isGoBackAvailable);
		cin >> input;
		switch (input[0])
		{
		case 'w':
			direction = data->UP;
			break;
		case 'a':
			direction = data->LEFT;
			break;
		case 's':
			direction = data->DOWN;
			break;
		case 'd':
			direction = data->RIGHT;
			break;
		case 'p':
			isOver = true;
		default:
			direction = data->NO_MOVE;
		}

		if (direction == data->NO_MOVE)
			continue;

		isMoved = data->moveArray(direction);

		if (!isMoved)
			continue;

		updateGameData();
		isOver = evaluateGameProgress();
	}
	
	endGame(input[0] == 'p');
}

//end game and deconstruct data
void User::endGame(bool isExit) 
{	
	if (!isExit)
	{	
		displayGame(isGoBackAvailable);
		cout << (data->getScore() >= 2048 ? "You Win!" : "You Lose...") << endl << endl;
		system("pause");
	}

	delete data;
	data = nullptr;
}

//update game data after each step
void User::updateGameData()
{
	data->updateStep();
	data->updateScore();
	data->addRandomValue();
}

//evaluate if the game should end
//return true to indicate the game should be ended
bool User::evaluateGameProgress()
{
	if (data->getScore() >= TARGET_SCORE)
		return true;
		
	if (data->isGridFull())
	{
		GameData temp = *data;
		if (!data->moveArray(data->UP) && !data->moveArray(data->LEFT) && !data->moveArray(data->DOWN) && !data->moveArray(data->RIGHT))
			return true;
		*data = temp;
	}

	return false;
}
