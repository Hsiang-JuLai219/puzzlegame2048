/*===============================================================*/
/* File Name: User.h									         */
/* Author: Hsiang-Ju Lai									     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: The header file defining Class User, which has	 */
/*	basic game control functions for Game 2048.					 */
/*===============================================================*/

#pragma once
#include<iostream>
#include<iomanip>
#include<cstdlib>
#include<string>
#include"GameData.h"
using namespace std;

class User
{
protected:
	bool isGoBackAvailable;
	GameData *data;
	void displayGame(bool isGobackable);
	void endGame(bool isExit);
	void startGame();
	void printLine();
	void updateGameData();
	bool evaluateGameProgress();

public:
	const int TARGET_SCORE = 2048;
	User();
	void userMenu();
};



