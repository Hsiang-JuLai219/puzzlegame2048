/*===============================================================*/
/* File Name: RankingList.h								         */
/* Author: Hsiang-Ju Lai									     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: The header file that defines Class RankingList,  */
/*	which is in charge of the ranking list for the game.		 */
/*===============================================================*/

#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iomanip>

using namespace std;

struct Record
{
	string username;
	int score;
	int steps;
	
	Record(string username, int score, int steps):username(username), score(score), steps(steps){}

	bool operator==(const Record& rhs) const
	{
		return score == rhs.score && steps == rhs.steps;
	}
	bool operator<(const Record& rhs) const
	{
		return (score > rhs.score) || (score == rhs.score &&  steps < rhs.steps) || (*this == rhs && username < rhs.username);
	}
};

class RankingList
{
private:
	const string FILE_NAME = "rankingList.txt";
	vector<Record> list;
	void saveList();
	void loadList();
	int findRecordIndex(string username);

public:
	RankingList();
	void updateUserRecord(string username, int score, int steps);
	void displayRankingList();
};