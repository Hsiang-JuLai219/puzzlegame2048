/*===============================================================*/
/* File Name: AccountSystem.h							         */
/* Author: Hsiang-Ju Lai									     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: The header file defining Class AccountSystem,	 */
/* which manages user accounts information independently.     	 */
/*===============================================================*/

#pragma once
#include "AdvancedUser.h"

using namespace std;

class AccountSystem
{
private:
	const string FILE_NAME = "userInfo.txt";
	vector<string> accountList;
	void saveList();
	void loadList();
	bool checkUsername(string username);
	bool checkPassword(string username, string password);

public:
	AccountSystem();
	AdvancedUser* signIn(RankingList *rankingList);
	void signUp();
	
};