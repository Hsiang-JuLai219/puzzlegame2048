/*===============================================================*/
/* File Name: RankingList.cpp							         */
/* Author: Hsiang-Ju Lai, Eric Hsu							     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: Implementation of Class RankingList			     */
/*===============================================================*/

#include "RankingList.h"

RankingList::RankingList() {loadList();}

//update user record and save to file
void RankingList::updateUserRecord(string username, int score, int steps)
{
	int index = findRecordIndex(username);
	if (index < 0)
		list.push_back(Record(username, score, steps));
	else
	{
		list[index].score = score;
		list[index].steps = steps;
	}

	sort(list.begin(), list.end());
	saveList();
}

//display ranking list to console
void RankingList::displayRankingList()
{
	system("cls");
	cout << endl << setw(45) << "Ranking List" << endl << endl;
	cout << setw(20) << "Rank #" << setw(15) << "Name" << setw(15) << "Score" << setw(15) << "# Step" << endl;
	cout << setw(75) << "________________________________________________________________________" << endl << endl;

	if (list.empty())
	{
		cout << "Ranking list is empty...\n" << endl;
	}
	else
	{
		int rank = 1, step = 1;
		for (unsigned int i = 0; i < list.size(); i++)
		{
			cout << setw(17) << rank << setw(19) << list[i].username 
				<< setw(12) << list[i].score << setw(15) << list[i].steps << endl;
			if (i + 1 < list.size() && list[i + 1] == list[i])
				step++;
			else
			{
				rank += step;
				step = 1;
			}
		}
		cout << endl << endl;
	}
	system("pause");
}

//find the record of the given username
//return -1 if not found
int RankingList::findRecordIndex(string username)
{
	for (unsigned int i = 0; i < list.size(); i++)
		if (list[i].username == username)
			return i;
	return -1;
}

//save the ranking list to file
void RankingList::saveList()
{
	ofstream fout(FILE_NAME.c_str());
	if (fout.fail())
	{
		cerr << "Fail to save ranking list!" << endl;
		system("pause");
		return;
	}

	for (unsigned int i = 0; i < list.size(); i++)
		fout << list[i].username << " " << list[i].score << " " << list[i].steps << endl;
}

//load the ranking list from file
void RankingList::loadList()
{
	ifstream fin(FILE_NAME.c_str());
	if (fin.fail())
		return;

	list.clear();

	string username;
	int score, steps;

	while (fin >> username >> score >> steps)
		list.push_back(Record(username, score, steps));
}