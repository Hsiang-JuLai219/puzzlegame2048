/*===============================================================*/
/* File Name: GameData.h								         */
/* Author: Hsiang-Ju Lai									     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: The header file that defines Class GameData,     */
/*	which manages the information of each game in progress.		 */
/*===============================================================*/

#pragma once
#include<iostream>
#include<iomanip>
#include<vector>
#include<cstdlib>

using namespace std;

class GameData
{
private:
	static const int NROWS = 4;
	static const int NCOLS = 4;

	int score;
	int step;
	int grid[NROWS][NCOLS]{ {0} };

	int findMax();
	vector<int> findEmptySpots();

public:
	const static int UP = 1, LEFT = 2,
		DOWN = 3, RIGHT = 4, NO_MOVE = 0;
	GameData();
	void updateScore();
	void updateStep();
	const int getStep();
	const int getScore();

	bool isGridFull();
	bool addRandomValue();
	bool moveArray(int dir);

	void printArray();
};


