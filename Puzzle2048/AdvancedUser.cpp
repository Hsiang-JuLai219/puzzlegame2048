/*===============================================================*/
/* File Name: AdvancedUser.cpp							         */
/* Author: Hsiang-Ju Lai, Eric Hsu							     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: Implementation of Class AdvancedUser.		     */
/*===============================================================*/

#include "AdvancedUser.h"
using namespace std;

//constructor for AdvancedUser with username and address of
//rankinglist as parameters
AdvancedUser::AdvancedUser(string username, RankingList *rankingList) : User()
{
	isGoBackAvailable = true;
	this->username = username;
	loadHistory();
	this->rankingList = rankingList;
}

//go back to previous step if exists
void AdvancedUser::goBack()
{
	if (history.empty())
		return;
	*data = history.top();
	history.pop();
}

//update and save user history
void AdvancedUser::updateHistory(int score, int step)
{	
	if (isBestScore(score, step))
		rankingList->updateUserRecord(username, score, step);

	vector<int> newRecord(2);
	newRecord[0] = score;
	newRecord[1] = step;
	records.push_back(newRecord);

	ofstream fout(username.c_str());
	if (fout.fail())
	{
		cerr << "Fail to save user records!" << endl;
		system("pause");
		return;
	}

	vector<vector<int>>::iterator i;
	for (i = records.begin(); i != records.end(); i++)
		fout << (*i)[0] << " " << (*i)[1] << endl;
}

//load the user's history from the file
void AdvancedUser::loadHistory()
{
	ifstream fin(username.c_str());
	if (fin.fail())
		return;

	records.clear();
	vector<int> record(2);
	while (fin >> record[0] >> record[1])
		records.push_back(record);
}

//show user his/her game history on console
void AdvancedUser::displayHistory()
{
	system("cls");

	if (records.empty())
	{
		cout << "You have no records..." << endl << endl;
		system("pause");
		return;
	}

	cout << endl << setw(35) << username << "'s score history";
	cout << endl << endl ;
	cout << setw(28) << "Game #" << setw(14) << "Score" << setw(16) << "# Steps" << endl;
	cout << setw(72) << "_______________________________________________________________" << endl << endl;
	for (unsigned int i = 0; i < records.size(); i++)
		cout << setw(26) << i + 1 << setw(15) << records[i][0] << setw(14) << records[i][1] << endl;

	cout << endl << endl;
	system("pause");
}

//show user his/her best score
void AdvancedUser::showBestScore()
{
	system("cls");
	
	if (records.empty())
	{
		cout << "You have no records...\n" << endl;
		system("pause");
		return;
	}

	int bestRecordIndex = findBestScore();
	
	cout << "Your Best Score is...\n" << endl;

	cout << setw(20) << "Game # " << bestRecordIndex + 1 << "   Score: " << records[bestRecordIndex][0]
		<< "    Number of Steps: " << records[bestRecordIndex][1] << endl << endl << endl;

	system("pause");
}

//return the index of the user's best score
int AdvancedUser::findBestScore()
{
	if (records.empty())
		return -1;
	
	int bestRecordIndex = 0;
	for (unsigned int i = 1; i < records.size(); i++)
		if (records[i][0] > records[bestRecordIndex][0])
			bestRecordIndex = i;
		else if (records[i][0] == records[bestRecordIndex][0] && records[i][1] < records[bestRecordIndex][1])
			bestRecordIndex = i;

	return bestRecordIndex;
}

//check if the passed-in record is the best score of the user
bool AdvancedUser::isBestScore(int score, int steps)
{
	int bestRecordIndex = findBestScore();
	if (bestRecordIndex == -1)
		return true;
	if (score > records[bestRecordIndex][0])
		return true;
	if (score == records[bestRecordIndex][0] && steps < records[bestRecordIndex][1])
		return true;

	return false;
}

//start the gama
void AdvancedUser::startGame()
{
	bool isOver(false), isMoved(false);
	string input;
	int direction;
	data = new GameData();

	while (!isOver)
	{
		displayGame(isGoBackAvailable);

		cin >> input;
		switch (input[0])
		{
		case 'w':
			direction = data->UP;
			break;
		case 'a':
			direction = data->LEFT;
			break;
		case 's':
			direction = data->DOWN;
			break;
		case 'd':
			direction = data->RIGHT;
			break;
		case 'b':
			goBack();
			direction = data->NO_MOVE;
			break;
		case 'p':
			isOver = true;
		default:
			direction = data->NO_MOVE;
		}
		
		if (direction == data->NO_MOVE)
			continue;

		history.push(*data);
		isMoved = data->moveArray(direction);

		if (!isMoved)
			continue;

		updateGameData();
		isOver = evaluateGameProgress();
	}

	updateHistory(data->getScore(), data->getStep());

	endGame(input[0] == 'p');
	//deconstruct data
}

//display advanced user menu on console
void AdvancedUser::userMenu()
{
	string input;
	while (true)
	{
		system("cls");
		cout << "\n\n\n\n\n\n\n\n\n";
		cout << setw(52) << " " << "     A   A      /" << endl;
		cout << setw(52) << " " << " ( = O w O = )    I want to play a game ~~" << endl;
		cout << setw(52) << " " << " (   U   U   )  \\" << endl;
		cout << setw(47) << "Advanced User" << "     ~(           )" << endl;
		cout << "   __________________________________________________(_____)_____)________________";

		cout << "\n\n";
		cout << "				1 ->> Start Game\n\n";
		cout << "				2 ->> Score History\n\n";
		cout << "				3 ->> Best Score\n\n";
		cout << "				4 ->> Exit\n\n";
		cout << "\n\n\n\n\n\n";

		cin >> input;

		switch (input[0])
		{
		case '1':
			startGame();
			break;
		case '2': 
			displayHistory();
			break;
		case '3':
			showBestScore();
			break;
		case '4': return;
			break;
		}
	}
}