/*===============================================================*/
/* File Name: GameData.cpp								         */
/* Author: Hsiang-Ju Lai, Eric Hsu							     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: Implementation of Class GameData.			     */
/*===============================================================*/

#include "GameData.h"
using namespace std;

//default constructor for GameData
GameData::GameData():step(0)
{
	addRandomValue();
	addRandomValue();
	score = findMax();
}

//"move" the array according to game logic
//return if the array is moved
bool GameData::moveArray(int dir)
{
	bool isMoved(false);
	int previous, pos, tempGrid[NROWS][NCOLS] = { {0} };

	switch (dir)
	{
	case UP:
		for (int j = 0; j < NCOLS; j++)
		{
			pos = 0;
			previous = -1;

			for (int i = 0; i < NROWS; i++)
			{
				if (grid[i][j] == 0)
					continue;

				if (previous == -1)
					previous = grid[i][j];
				else if (previous == grid[i][j])
				{
					tempGrid[pos++][j] = previous * 2;
					previous = -1;
				}
				else
				{
					tempGrid[pos++][j] = previous;
					previous = grid[i][j];
				}
			}

			if (previous != -1)
				tempGrid[pos][j] = previous;
		}
		break;

	case DOWN:
		for (int j = 0; j < NCOLS; j++)
		{
			pos = NROWS - 1;
			previous = -1;

			for (int i = NROWS - 1; i >= 0; i--)
			{
				if (grid[i][j] == 0)
					continue;

				if (previous == -1)
					previous = grid[i][j];
				else if (previous == grid[i][j])
				{
					tempGrid[pos--][j] = previous * 2;
					previous = -1;
				}
				else
				{
					tempGrid[pos--][j] = previous;
					previous = grid[i][j];
				}
			}

			if (previous != -1)
				tempGrid[pos][j] = previous;
		}
		break;

	case LEFT:
		for (int i = 0; i < NROWS; i++)
		{
			pos = 0;
			previous = -1;

			for (int j = 0; j < NCOLS; j++)
			{
				if (grid[i][j] == 0)
					continue;

				if (previous == -1)
					previous = grid[i][j];
				else if (previous == grid[i][j])
				{
					tempGrid[i][pos++] = previous * 2;
					previous = -1;
				}
				else
				{
					tempGrid[i][pos++] = previous;
					previous = grid[i][j];
				}
			}
			if (previous != -1)
				tempGrid[i][pos] = previous;
		}
		break;

	case RIGHT:
		for (int i = 0; i < NROWS; i++)
		{
			pos = NCOLS - 1;
			previous = -1;

			for (int j = NCOLS - 1; j >= 0; j--)
			{
				if (grid[i][j] == 0)
					continue;

				if (previous == -1)
					previous = grid[i][j];
				else if (previous == grid[i][j])
				{
					tempGrid[i][pos--] = previous * 2;
					previous = -1;
				}
				else
				{
					tempGrid[i][pos--] = previous;
					previous = grid[i][j];
				}
			}
			if (previous != -1)
				tempGrid[i][pos] = previous;
		}
		break;

	default: return false;
	}

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			if (grid[i][j] != tempGrid[i][j])
			{
				isMoved = true;
				grid[i][j] = tempGrid[i][j];
			}
				
	return isMoved;
}

//insert a random number n in grid
bool GameData::addRandomValue()
{
	vector<int> spots = findEmptySpots();

	if (spots.empty())
		return false;

	int spot = spots[rand() % spots.size()];
	grid[spot/4][spot%4] = rand() % 10 >= 8 ? 4 : 2;

	return true;
}

const int GameData::getScore() { return score; }
const int GameData::getStep() { return step; }

void GameData::updateScore() { score = findMax(); }
void GameData::updateStep() { step++; }

//output the array of grid to console
void GameData::printArray()
{
	cout << setw(58);
	cout << "---------------------------------------\n";

	for (int i = 0; i < NROWS; i++)
	{
		cout << setw(59);
		cout << "|         |         |         |         |\n";
		cout << setw(18);
		cout << "|";

		for (int j = 0; j < NCOLS; j++) {
			if (grid[i][j] == 0)
				cout << setw(5) << " " << setw(5);
			else if (grid[i][j] > 100)
				cout << setw(6) << grid[i][j] << setw(4);
			else
				cout << setw(5) << grid[i][j] << setw(5);
			cout << "|";
		}

		cout << endl;
		cout << setw(59);
		cout << "|         |         |         |         |\n";
		if (i < NROWS - 1) {
			cout << setw(58);
			cout << "|---------|---------|---------|---------|";
			cout << endl;
		}
	}
	cout << setw(58);
	cout << "---------------------------------------\n";
}


//find entries with value of 0 in the grid
//return a vector containing all indexes of value 0
vector<int> GameData::findEmptySpots()
{
	vector<int> spots;
	spots.reserve(16);
	for (int i = 0; i < NROWS; i++)
		for (int j = 0; j < NCOLS; j++)
			if (grid[i][j] == 0)
				spots.push_back(j + 4 * i);

	return spots;	
}

//get max number(score) in grid
int GameData::findMax() 
{
	int max = 0;
	for (int i = 0; i < NROWS; i++)
		for (int j = 0; j < NCOLS; j++)
			if (grid[i][j] > max)
				max = grid[i][j];

	return max;
}

//return if the gird is full
bool GameData::isGridFull() {return findEmptySpots().empty();}
