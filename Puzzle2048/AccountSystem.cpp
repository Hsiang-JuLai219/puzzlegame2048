/*===============================================================*/
/* File Name: AccountSystem.cpp							         */
/* Author: Hsiang-Ju Lai									     */
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: Implementation of Class AccountSystem.		     */
/*===============================================================*/

#include "AccountSystem.h"

using namespace std;

//default constructor which loads the account list from file
AccountSystem::AccountSystem()
{
	loadList();
}

//save the account list to file
void AccountSystem::saveList()
{
	ofstream fout(FILE_NAME.c_str());
	if (fout.fail())
		return;

	vector<string>::iterator i;
	for (i = accountList.begin(); i != accountList.end(); i += 2)
		fout << *i << " " << *(i+1) << endl;
}

//load the account list from file
void AccountSystem::loadList()
{
	ifstream fin(FILE_NAME.c_str());
	string username, password;
	if (fin.fail())
		return;

	accountList.clear();

	while (fin >> username >> password)
	{
		accountList.push_back(username);
		accountList.push_back(password);
	}

	fin.close();
}

//check if the username exists
bool AccountSystem::checkUsername(string username)
{
	vector<string>::iterator i;
	for (i = accountList.begin(); i != accountList.end(); i += 2)
		if (*i == username)
			return true;
	return false;
}

//check if the username and password match
bool AccountSystem::checkPassword(string username, string password)
{
	vector<string>::iterator i;
	for (i = accountList.begin(); i != accountList.end(); i += 2)
		if (*i == username)
			if (*(i+1) == password)
				return true;
	return false;
}

//porvide the user an interface to sign in
AdvancedUser* AccountSystem::signIn(RankingList *rankingList)
{
	string username, password;
	system("cls");

	cout << endl;

	cout << "	" << "  A A\n";
	cout << "	" << "(" << char(240) << char(249) << "_" 
		<< char(249) << " " << char(240) << " )  "
		<< setw(24) << "What's you're name ? ";
	cin >> username;
	cout << endl;

	cout << "	" << "    A A\n";
	cout << "	( " << char(240) << " " << char(249) << "_" 
		<< char(249) << char(240) << ") ?" << setw(28) 
		<< "What's you're password ? ";
	cin >> password;

	if (checkPassword(username, password))
		return new AdvancedUser(username, rankingList);

	cout << "\n\n	Username and password do not match!\n\n";
	cout << "	\\ (" << char(240) << " >n< " << char(240) 
		<< ") /  I don't know you !!\n\n" << endl;
	system("pause");

	return nullptr;
}

//let the user sign up a new account as advanced user
void AccountSystem::signUp()
{
	bool done(false);
	string username, password;
	system("cls");

	cout << endl << endl;
	cout << "	\\(" << char(249) << "w" << char(249) << ")/" 
		<< setw(40) << "Welcome to the world of 2048 !!"
		<< setw(10) << "\\(" << char(249) << "w" << char(249) 
		<< ")/\n" << endl;

	while (!done)
	{	
		cout << setw(30) << "Enter username : ";
		cin >> username;
		cout << endl;
		cout << setw(30) << "Enter password : ";
		cin >> password;

		if (checkUsername(username))
		{
			cout << "\n\n	" << "(" << char(210) << "_" << char(210)
				<< ")	Username already exists! " 
				<< "Please try another one." << setw(5) << "("
				<< char(210) << "_" << char(210) << ")\n" << endl;
		}
		else
		{
			accountList.push_back(username);
			accountList.push_back(password);
			saveList();
			cout << "\n	  A A";
			cout << "\n	( ^_^) /" << setw(33)
				<< "Profile Created Successfully!\n\n" << endl;
			done = true;
			system("pause");
		}
	}
}