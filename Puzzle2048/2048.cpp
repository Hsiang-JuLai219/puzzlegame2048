/*===============================================================*/
/* File Name: 2048.cpp										     */
/* Author: Hsiang-Ju Lai, Eric Hsu							     */	
/* Platform: Windows 10 x64/x86 Visual Studio 2015				 */
/* Version: 1.0													 */
/* Description: This is the driver of a puzzle game named 2048.  */
/*===============================================================*/

#include "User.h"
#include "AdvancedUser.h"
#include "AccountSystem.h"
#include "RankingList.h"

using namespace std;

int main()
{
	srand((unsigned int)time(NULL));
	bool exit(false);
	string input;
	
	AccountSystem accounts;
	User trialUser;
	AdvancedUser *user;
	RankingList rankingList;

	while (!exit)
	{
		system("cls");
		cout << "\n\n";
		cout << setw(80) << " ========================================================================= \n";
		cout << setw(80) << "|      ******       *******     *       *     *******    *******          |\n";
		cout << setw(80) << "|     ********     *********    **     **    *********   **               |\n";
		cout << setw(80) << "|    **      **    **     **    **     **    **     **   *******          |\n";
		cout << setw(80) << "|            **    **     **    **     **    **     **   **               |\n";
		cout << setw(80) << "|     ********     **     **    *********     *******    *******          |\n";
		cout << setw(80) << "|    ********      **     **     ********     *******          **    **   |\n";
		cout << setw(80) << "|    **            **     **           **    **     **          **  **    |\n";
		cout << setw(80) << "|    **            **     **           **    **     **           ****     |\n";
		cout << setw(80) << "|    **********    *********           **    *********          **  **    |\n";
		cout << setw(80) << "|     ********      *******            *      *******          **    **   |\n";
		cout << setw(80) << " ========================================================================= \n";
		cout << "\n";
		cout << "   ______________________________________________________________________________";
		cout << "\n\n";
		cout << "				" << "1 ->>  QUICK START\n\n";
		cout << "				" << "2 ->>  SIGN IN                      A   A    \n";
		cout << "				" << "                                (           )\n";
		cout << "				" << "3 ->>  SIGN UP                  (           )\n";
		cout << "				" << "                                (      ~~   )\n";
		cout << "				" << "4 ->>  RANKING LIST             (_____)(____)\n\n";
		cout << "				" << "5 ->>  EXIT\n\n";
		cout << endl << setw(80) << "Eric Hsu, Hsiang-Ju Lai 2016, Version 1.0EX\n";

		cin >> input;
		switch (input[0])
		{
		case '1': //start as trail user
			trialUser.userMenu();
			break;

		case '2': //sign in as advanced user
			user = accounts.signIn(&rankingList);
			if (user != nullptr)
			{
				user->userMenu();
				delete user;
				user = nullptr;
			}
			break;

		case '3': //sign up to be an advanced user
			accounts.signUp();
			break;

		case '4': //display ranking list
			rankingList.displayRankingList();
			break;

		case '5': //exit game
			exit = true;
			break;
		}
	}
	return 0;
}